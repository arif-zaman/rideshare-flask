import json
import sqlite3
from contextlib import closing

from flask import Flask, request, g, jsonify
from werkzeug.security import generate_password_hash, check_password_hash

# Configuration
DATABASE = 'rideshare.db'
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'rideshare'
PASSWORD = "rideshare"

# ROLE
Role = ['ADMIN', 'PASSENGER', 'DRIVER', 'COMPANY']

# APP
app = Flask(__name__)
app.config.from_object(__name__)


# DATABASE CONNECTION
def connectDB():
    return sqlite3.connect(app.config['DATABASE'])


def initDB():
    with closing(connectDB()) as db:
        with app.open_resource('rideshare.sql', mode='r') as f:
            '''
            schema = f.readlines()
            for line in schema:
                print line
            '''
            db.cursor().executescript(f.read())

        db.commit()


@app.before_request
def beforeRequest():
    g.db = connectDB()


@app.teardown_request
def teardownrequest(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()


# ROUTING
@app.route("/")
def welcome():
    response = {'RideShare': 'Welcome to RideShare API'}
    return jsonify(response)


# ********** USER ************ #
def create_user(email, password, role):
    try:
        query = 'INSERT INTO USER (email,password,role) VALUES (?,?,?)'
        g.db.execute(query, [email, password, role])
        g.db.commit()
        return True
    except:
        return False


def get_user_id(email):
    try:
        query = 'SELECT id FROM USER WHERE email="' + email + '"'
        cur = g.db.execute(query)
        data = [dict(id=row[0]) for row in cur.fetchall()]
        return data[0]['id']
    except:
        return -1


def delete_user(id):
    print "Deleted USER : " + id
    try:
        query = 'DELETE FROM USER WHERE id=' + id
        g.db.execute(query)
        g.db.commit()
        return True
    except:
        return False


@app.route("/user/", methods=['GET', 'POST'])
def user():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if "email" in body:
            email = body['email']
        else:
            email = ""

        if "password1" in body:
            password1 = body['password1']
        else:
            password1 = ""

        if "password2" in body:
            password2 = body['password2']
        else:
            password2 = ""

        if 'role' in body:
            role = body['role']
        else:
            role = ''

        query = 'SELECT id FROM USER WHERE email="' + email + '"'
        cur = g.db.execute(query)

        if len(email) == 0:
            response['success'] = False
            response['message'] = "Enter Valid Email."

        elif password1 != password2:
            response['success'] = False
            response['message'] = "Password and Confirm Password Doesn't Match."

        elif len(password1) < 5:
            response['success'] = False
            response['message'] = "Password Length Must Be Atleast 5."

        elif role not in Role:
            response['success'] = False
            response['message'] = "Enter Valid Role"

        elif 0 < len(cur.fetchall()):
            response['success'] = False
            response['message'] = "User Already Exists!"

        else:
            password = generate_password_hash(password1)
            try:
                query = 'INSERT INTO USER (email,password,role) VALUES (?,?,?)'
                g.db.execute(query, [email, password, role])
                g.db.commit()
                response['success'] = True
                response['message'] = "New Entry Was Successfully Posted."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    elif request.method == "GET":
        try:
            query = 'SELECT id,email,role FROM USER'
            cur = g.db.execute(query)
            data = [dict(id=row[0], email=row[1], role=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " User's Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    else:
        response['success'] = False
        response['message'] = "HTTP Method Not Allowed! Send Valid Request."

    return json.dumps(response)


@app.route("/login/", methods=['POST'])
def login():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if "email" in body:
            email = body['email']
        else:
            email = ""

        if "password" in body:
            password = body['password']
        else:
            password = ""

        try:
            query = 'SELECT * FROM USER WHERE email="' + email + '"'
            cur = g.db.execute(query)
            data = [dict(id=row[0], email=row[1], password=row[2],
                         role=row[3]) for row in cur.fetchall()]
            data = data[0]

            if len(data) > 0:
                if check_password_hash(data['password'], password):
                    response['success'] = True
                    response['message'] = "Successfully Authenticated!"
                    del data['password']
                    response['data'] = data
                else:
                    response['success'] = False
                    response['message'] = "Authentication Failed"
            else:
                response['success'] = False
                response['message'] = "Authentication Failed"
        except:
            response['success'] = False
            response['message'] = "Database Error"

    return json.dumps(response)


@app.route("/user/<id>/", methods=['GET', 'PUT', 'DELETE'])
def User(id):
    response = {}
    if request.method == "GET":
        query = 'SELECT id,email,role FROM USER WHERE id=' + id
        cur = g.db.execute(query)
        data = [dict(id=row[0], email=row[1], role=row[2])
                for row in cur.fetchall()]

        if len(data) == 0:
            response['success'] = False
            response['message'] = "No User's Found."
        else:
            response['success'] = True
            response['data'] = data[0]

    elif request.method == "PUT":
        pass

    elif request.method == "DELETE":
        if delete_user(id):
            response['success'] = True
            response['message'] = "User ID = " + id + " Successfully Deleted."
        else:
            response['success'] = False
            response['message'] = "User Deletion Failed."

    else:
        response['success'] = False
        response['message'] = "HTTP Method Not Allowed! Send Valid Request."

    return json.dumps(response)


# ********** PASSENGER ************ #
@app.route("/passenger/", methods=['GET', 'POST'])
def passenger():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'name' in body:
            name = body['name']
        else:
            name = None

        if 'display_name' in body:
            display_name = body['display_name']
        else:
            if name:
                display_name = name
            else:
                display_name = None

        if "email" in body:
            email = body['email']
        else:
            email = ""

        if "password1" in body:
            password1 = body['password1']
        else:
            password1 = ""

        if "password2" in body:
            password2 = body['password2']
        else:
            password2 = ""

        if 'uni_id' in body:
            uni_id = body['uni_id']
        else:
            uni_id = None

        if 'com_id' in body:
            com_id = body['com_id']
        else:
            com_id = None

        if 'info' in body:
            info = body['info']
        else:
            info = None

        query = 'SELECT id FROM USER WHERE email="' + email + '"'
        cur = g.db.execute(query)
        role = 'PASSENGER'

        if len(email) < 1:
            response['success'] = False
            response['message'] = "Enter Valid Email."

        elif len(password1) < 5:
            response['success'] = False
            response['message'] = "Password Length Must Be Atleast 5."

        elif password1 != password2:
            response['success'] = False
            response['message'] = "Password and Confirm Password Doesn't Match."

        elif role not in Role:
            response['success'] = False
            response['message'] = "Enter Valid Role"

        elif 0 < len(cur.fetchall()):
            response['success'] = False
            response['message'] = "Email Already Exists!"

        else:
            password = generate_password_hash(password1)
            if create_user(email, password, role):
                user_id = get_user_id(email)
                print user_id
                if user_id == -1:
                    response['success'] = False
                    response['message'] = "User Profile Created! but Not Found!"
                else:
                    try:
                        query = 'INSERT INTO PASSENGER(name,display_name,email,uni_id,com_id,info,user_id) VALUES (?,?,?,?,?,?,?)'
                        g.db.execute(
                            query, [name, display_name, email, uni_id, com_id, info, user_id])
                        g.db.commit()
                        response['success'] = True
                        response[
                            'message'] = "User and Passenger Profile Successfully Created!"
                    except:
                        if delete_user(user_id):
                            response['success'] = False
                            response[
                                'message'] = "User Profile Created and Deleted As Passenger Profile Creation Failed!"
                        else:
                            response['success'] = False
                            response[
                                'message'] = "User Profile Deletion Failed!"
            else:
                response['success'] = False
                response['message'] = "User Profile Creation Failed!"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM PASSENGER'
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], display_name=row[2], email=row[3], uni_id=row[4], com_id=row[5],
                         photo=row[6], info=row[7], user_id=row[8]) for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Passenger's Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    return json.dumps(response)


@app.route("/passenger/<id>/", methods=['GET', 'PUT', 'DELETE'])
def Passenger(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM PASSENGER WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], display_name=row[2], email=row[3], uni_id=row[4], com_id=row[5],
                         photo=row[6], info=row[7], user_id=row[8]) for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data[0]
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "PUT":
        pass

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM PASSENGER WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** DRIVER ************ #
@app.route("/driver/", methods=['GET', 'POST'])
def driver():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'name' in body:
            name = body['name']
        else:
            name = None

        if 'display_name' in body:
            display_name = body['display_name']
        else:
            if name:
                display_name = name
            else:
                display_name = None

        if "email" in body:
            email = body['email']
        else:
            email = ""

        if "password1" in body:
            password1 = body['password1']
        else:
            password1 = ""

        if "password2" in body:
            password2 = body['password2']
        else:
            password2 = ""

        if 'reg_no' in body:
            reg_no = body['reg_no']
        else:
            reg_no = None

        if 'taxi_no' in body:
            taxi_no = body['taxi_no']
        else:
            taxi_no = None

        if 'info' in body:
            info = body['info']
        else:
            info = None

        query = 'SELECT id FROM USER WHERE email="' + email + '"'
        cur = g.db.execute(query)
        role = 'DRIVER'

        if len(email) < 1:
            response['success'] = False
            response['message'] = "Enter Valid Email."

        elif len(password1) < 5:
            response['success'] = False
            response['message'] = "Password Length Must Be Atleast 5."

        elif password1 != password2:
            response['success'] = False
            response['message'] = "Password and Confirm Password Doesn't Match."

        elif role not in Role:
            response['success'] = False
            response['message'] = "Enter Valid Role"

        elif len(cur.fetchall()) > 0:
            response['success'] = False
            response['message'] = "Email Already Exists!"

        else:
            password = generate_password_hash(password1)
            if create_user(email, password, role):
                user_id = get_user_id(email)
                print user_id
                if user_id == -1:
                    response['success'] = False
                    response['message'] = "User Profile Created! but Not Found!"
                else:
                    try:
                        query = 'INSERT INTO DRIVER(name,display_name,email,reg_no,taxi_no,info,user_id) VALUES (?,?,?,?,?,?,?)'
                        g.db.execute(
                            query, [name, display_name, email, reg_no, taxi_no, info, user_id])
                        g.db.commit()
                        response['success'] = True
                        response[
                            'message'] = "User and Driver Profile Successfully Created!"
                    except:
                        try:
                            query = 'DELETE FROM USER WHERE id=' + user_id
                            g.db.execute(query)
                            g.db.commit()
                            response['success'] = False
                            response[
                                'message'] = 'User Profile Created and Deleted.'
                        except:
                            response['success'] = False
                            response[
                                'message'] = 'User Profile Deletion Failed.'
            else:
                response['success'] = False
                response['message'] = "User Profile Creation Failed!"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM DRIVER'
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], display_name=row[2], email=row[3], reg_no=row[4], taxi_no=row[5],
                         photo=row[6], info=row[7], user_id=row[8]) for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    return json.dumps(response)


@app.route("/driver/<id>/", methods=['GET', 'PUT', 'DELETE'])
def Driver(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM DRIVER WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], display_name=row[2], email=row[3], reg_no=row[4], taxi_no=row[5],
                         photo=row[6], info=row[7], user_id=row[8]) for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data[0]
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "PUT":
        pass

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM DRIVER WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** COMPANY ************ #
@app.route("/company/", methods=["GET", "POST"])
def company():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'name' in body:
            name = body['name']
        else:
            name = None

        if "email" in body:
            email = body['email']
        else:
            email = ""

        if "password1" in body:
            password1 = body['password1']
        else:
            password1 = ""

        if "password2" in body:
            password2 = body['password2']
        else:
            password2 = ""

        if 'reg_no' in body:
            reg_no = body['reg_no']
        else:
            reg_no = None

        if 'info' in body:
            info = body['info']
        else:
            info = None

        query = 'SELECT id FROM USER WHERE email="' + email + '"'
        cur = g.db.execute(query)
        role = 'COMPANY'

        if len(email) < 1:
            response['success'] = False
            response['message'] = "Enter Valid Email."

        elif len(password1) < 5:
            response['success'] = False
            response['message'] = "Password Length Must Be Atleast 5."

        elif password1 != password2:
            response['success'] = False
            response['message'] = "Password and Confirm Password Doesn't Match."

        elif role not in Role:
            response['success'] = False
            response['message'] = "Enter Valid Role"

        elif len(cur.fetchall()) > 0:
            response['success'] = False
            response['message'] = "Email Already Exists!"

        else:
            password = generate_password_hash(password1)
            if create_user(email, password, role):
                user_id = get_user_id(email)
                print user_id
                if user_id == -1:
                    response['success'] = False
                    response['message'] = "User Profile Created! but Not Found!"
                else:
                    try:
                        query = 'INSERT INTO COMPANY(name,email,reg_no,info,user_id) VALUES (?,?,?,?,?)'
                        g.db.execute(
                            query, [name, email, reg_no, info, user_id])
                        g.db.commit()
                        response['success'] = True
                        response[
                            'message'] = "User and Company Profile Successfully Created!"
                    except:
                        try:
                            query = 'DELETE FROM USER WHERE id=' + user_id
                            g.db.execute(query)
                            g.db.commit()
                            response['success'] = False
                            response[
                                'message'] = 'User Profile Created and Deleted.'
                        except:
                            response['success'] = False
                            response[
                                'message'] = 'User Profile Deletion Failed.'
            else:
                response['success'] = False
                response['message'] = "User Profile Creation Failed!"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM COMPANY'
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], email=row[2], reg_no=row[3], logo=row[4], info=row[5], user_id=row[6])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    return json.dumps(response)


@app.route("/company/<id>/", methods=['GET', 'PUT', 'DELETE'])
def Company(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM COMPANY WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], email=row[2], reg_no=row[3], logo=row[4], info=row[5], user_id=row[6])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data[0]
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "PUT":
        pass

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM COMPANY WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** UNIVERSITY ************ #
@app.route("/university/", methods=["GET", "POST"])
def university():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'name' in body:
            name = body['name']
        else:
            name = ''

        if 'email_ext' in body:
            email_ext = body['email_ext']
        else:
            email_ext = ""

        if len(name) == 0 or len(email_ext) == 0:
            response['success'] = False
            response[
                'message'] = "University Name and Email Extention are Required."

        else:
            try:
                query = 'INSERT INTO UNIVERSITY (name,email_ext) VALUES (?,?)'
                g.db.execute(query, [name, email_ext])
                g.db.commit()
                response['success'] = True
                response['message'] = "New University Successfully Added."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM UNIVERSITY'
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], email_ext=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    else:
        response['success'] = False
        response['message'] = "HTTP Method Not Allowed! Send Valid Request."

    return json.dumps(response)


@app.route("/university/<id>/", methods=['GET', 'PUT', 'DELETE'])
def University(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM UNIVERSITY WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], email_ext=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data[0]
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "PUT":
        pass

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM UNIVERSITY WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** COMMUNITY ************ #
@app.route("/community/", methods=["GET", "POST"])
def community():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'name' in body:
            name = body['name']
        else:
            name = ''

        if 'uni_id' in body:
            uni_id = body['uni_id']
        else:
            uni_id = ""

        if len(name) == 0 or len(uni_id) == 0:
            response['success'] = False
            response['message'] = "Community Name and University ID are Required."

        else:
            try:
                query = 'INSERT INTO COMMUNITY (name,uni_id) VALUES (?,?)'
                g.db.execute(query, [name, uni_id])
                g.db.commit()
                response['success'] = True
                response['message'] = "New Community Successfully Added."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM COMMUNITY'
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], uni_id=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Community's Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    else:
        response['success'] = False
        response['message'] = "HTTP Method Not Allowed! Send Valid Request."

    return json.dumps(response)


@app.route("/community/<id>/", methods=['GET', 'PUT', 'DELETE'])
def Community(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM COMMUNITY WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1], uni_id=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data[0]
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "PUT":
        pass

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM COMMUNITY WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** location ************ #
@app.route("/location/", methods=["GET", "POST"])
def location():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'name' in body:
            name = body['name']
        else:
            name = ''

        if len(name) == 0:
            response['success'] = False
            response['message'] = "Location Name is Required."

        else:
            try:
                query = 'INSERT INTO LOCATION (name) VALUES (?)'
                g.db.execute(query, [name])
                g.db.commit()
                response['success'] = True
                response['message'] = "New Location Successfully Added."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM LOCATION'
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1]) for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Location's Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    else:
        response['success'] = False
        response['message'] = "HTTP Method Not Allowed! Send Valid Request."

    return json.dumps(response)


@app.route("/location/<id>/", methods=['GET', 'PUT', 'DELETE'])
def Location(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM LOCATION WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], name=row[1]) for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data[0]
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "PUT":
        pass

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM LOCATION WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** RIDE ************ #
@app.route("/ride/", methods=["GET", "POST"])
def ride():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'creator_id' in body:
            creator_id = body['creator_id']
        else:
            creator_id = None

        if 'start_loc_id' in body:
            start_loc_id = body['start_loc_id']
        else:
            start_loc_id = None

        if 'end_loc_id' in body:
            end_loc_id = body['end_loc_id']
        else:
            end_loc_id = None

        if 'time' in body:
            time = body['time']
        else:
            time = '2016-02-07 23:23:23'

        if 'pass_required' in body:
            pass_required = body['pass_required']
        else:
            pass_required = None

        if 'driver_id' in body:
            driver_id = body['driver_id']
        else:
            driver_id = None

        if 'complete' in body:
            complete = body['complete']
        else:
            complete = '2016-02-07 23:23:23'

        if True:
            try:
                query = 'INSERT INTO RIDE (creator_id,start_loc_id,end_loc_id,time,pass_required,driver_id,complete) VALUES (?,?,?,?,?,?,?)'
                g.db.execute(query, [
                             creator_id, start_loc_id, end_loc_id, time, pass_required, driver_id, complete])
                g.db.commit()
                response['success'] = True
                response['message'] = "New Ride Successfully Added."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM RIDE ORDER BY creator_id'
            cur = g.db.execute(query)
            data = [dict(id=row[0], creator_id=row[1], start_loc_id=row[2], end_loc_id=row[3], time=row[4],
                         pass_required=row[5], pass_on_board=row[6], driver_id=row[7], active=row[8], complete=row[9])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    return json.dumps(response)


@app.route("/ride/<id>/", methods=['GET', 'PUT', 'DELETE'])
def Ride(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM RIDE WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], creator_id=row[1], start_loc_id=row[2], end_loc_id=row[3], time=row[4],
                         pass_required=row[5], pass_on_board=row[6], driver_id=row[7], active=row[8], complete=row[9])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data[0]
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "PUT":
        pass

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM RIDE WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** invitation ************ #
@app.route("/invitation/", methods=["GET", "POST"])
def invitation():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'creator_id' in body:
            creator_id = body['creator_id']
        else:
            creator_id = None

        if 'receiver_id' in body:
            receiver_id = body['receiver_id']
        else:
            receiver_id = None

        if 'start_loc_id' in body:
            start_loc_id = body['start_loc_id']
        else:
            start_loc_id = None

        if 'end_loc_id' in body:
            end_loc_id = body['end_loc_id']
        else:
            end_loc_id = None

        if 'time' in body:
            time = body['time']
        else:
            time = '2016-02-07 23:23:23'

        if 'created' in body:
            created = body['created']
        else:
            created = '2016-02-07 23:23:23'

        if True:
            try:
                query = 'INSERT INTO INVITATION (creator_id,receiver_id,start_loc_id,end_loc_id,time,created) VALUES (?,?,?,?,?,?)'
                g.db.execute(query, [creator_id, receiver_id,
                                     start_loc_id, end_loc_id, time, created])
                g.db.commit()
                response['success'] = True
                response['message'] = "New Invitation Successfully Added."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM INVITATION ORDER BY creator_id'
            cur = g.db.execute(query)
            data = [dict(id=row[0], creator_id=row[1], receiver_id=row[2], start_loc_id=row[3], end_loc_id=row[4],
                         time=row[5], accept=row[6], created=row[7]) for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    return json.dumps(response)


@app.route("/invitation/<id>/", methods=['GET', 'PUT', 'DELETE'])
def Invitation(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM INVITATION WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], creator_id=row[1], receiver_id=row[2], start_loc_id=row[3], end_loc_id=row[4],
                         time=row[5], accept=row[6], created=row[7]) for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data[0]
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "PUT":
        pass

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM INVITATION WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** RIDE_LOG ************ #
@app.route("/log/ride/", methods=["GET", "POST"])
def ride_log():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'ride_id' in body:
            ride_id = body['ride_id']
        else:
            ride_id = 0

        if 'pass_id' in body:
            pass_id = body['pass_id']
        else:
            pass_id = 0

        if ride_id == 0 or pass_id == 0:
            response['success'] = False
            response['message'] = "Ride ID and Passenger ID are Required."

        else:
            try:
                query = 'INSERT INTO RIDE_LOG (ride_id,pass_id) VALUES (?,?)'
                g.db.execute(query, [ride_id, pass_id])
                g.db.commit()
                response['success'] = True
                response['message'] = "New Ride Log Successfully Added."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM RIDE_LOG ORDER BY ride_id'
            cur = g.db.execute(query)
            data = [dict(id=row[0], ride_id=row[1], pass_id=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    else:
        response['success'] = False
        response['message'] = "HTTP Method Not Allowed! Send Valid Request."

    return json.dumps(response)


@app.route("/log/ride/<id>/", methods=['GET', 'PUT', 'DELETE'])
def Ride_log(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM RIDE_LOG WHERE id=' + id + ' ORDER BY ride_id'
            cur = g.db.execute(query)
            data = [dict(id=row[0], ride_id=row[1], pass_id=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM RIDE_LOG WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** RIDE_APPLY_PASSENGER ************ #
@app.route("/apply/passenger/ride/", methods=["GET", "POST"])
def apply_passenger():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'ride_id' in body:
            ride_id = body['ride_id']
        else:
            ride_id = 0

        if 'pass_id' in body:
            pass_id = body['pass_id']
        else:
            pass_id = 0

        if (ride_id == 0 or pass_id == 0):
            response['success'] = False
            response['message'] = "Ride ID and Passenger ID are Required."

        else:
            try:
                query = 'INSERT INTO RIDE_APPLY_PASSENGER (ride_id,pass_id) VALUES (?,?)'
                g.db.execute(query, [ride_id, pass_id])
                g.db.commit()
                response['success'] = True
                response['message'] = "Successfully Added."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM RIDE_APPLY_PASSENGER ORDER BY ride_id'
            cur = g.db.execute(query)
            data = [dict(id=row[0], ride_id=row[1], pass_id=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    return json.dumps(response)


@app.route("/apply/passenger/ride/<id>/", methods=['GET', 'DELETE'])
def Apply_passenger(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM RIDE_APPLY_PASSENGER WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], ride_id=row[1], pass_id=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM RIDE_APPLY_PASSENGER WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** RIDE_APPLY_DRIVER ************ #
@app.route("/apply/driver/ride/", methods=["GET", "POST"])
def apply_driver():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'ride_id' in body:
            ride_id = body['ride_id']
        else:
            ride_id = 0

        if 'driver_id' in body:
            driver_id = body['driver_id']
        else:
            driver_id = 0

        if len(ride_id) == 0 or len(driver_id) == 0:
            response['success'] = False
            response['message'] = "Ride ID and Driver ID are Required."

        else:
            try:
                query = 'INSERT INTO RIDE_APPLY_DRIVER (ride_id,driver_id) VALUES (?,?)'
                g.db.execute(query, [ride_id, driver_id])
                g.db.commit()
                response['success'] = True
                response['message'] = "Successfully Added."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    elif request.method == "GET":
        try:
            query = 'SELECT * FROM RIDE_APPLY_DRIVER ORDER BY ride_id'
            cur = g.db.execute(query)
            data = [dict(id=row[0], ride_id=row[1], driver_id=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    return json.dumps(response)


@app.route("/apply/driver/ride/<id>/", methods=['GET', 'DELETE'])
def Apply_driver(id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM RIDE_APPLY_DRIVER WHERE id=' + id
            cur = g.db.execute(query)
            data = [dict(id=row[0], ride_id=row[1], driver_id=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data[0]
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM RIDE_APPLY_DRIVER WHERE id=' + id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


# ********** NOTIFICATION ************ #
@app.route("/notification/", methods=["POST"])
def notification():
    response = {}
    if request.method == "POST":
        body = request.get_json(silent=True)

        if 'user_id' in body:
            user_id = body['user_id']
            if (len(str(user_id)) == 0):
                user_id = 0
        else:
            user_id = 0

        if 'body' in body:
            body = body['body']
        else:
            body = ""

        if (user_id == 0 or len(body) == 0):
            response['success'] = False
            response['message'] = "User ID and Notification Body are Required."

        else:
            try:
                query = 'INSERT INTO NOTIFICATION (user_id,body) VALUES (?,?)'
                g.db.execute(query, [user_id, body])
                g.db.commit()
                response['success'] = True
                response['message'] = "Successfully Added."
            except:
                response['success'] = False
                response['message'] = "Database Error"

    return json.dumps(response)


@app.route("/notification/<user_id>/", methods=['GET', 'DELETE'])
def Notification(user_id):
    response = {}
    if request.method == "GET":
        try:
            query = 'SELECT * FROM NOTIFICATION WHERE user_id=' + user_id
            cur = g.db.execute(query)
            data = [dict(id=row[0], user_id=row[1], body=row[2])
                    for row in cur.fetchall()]
            response['success'] = True
            response['message'] = str(len(data)) + " Results Found."
            response['data'] = data
        except:
            response['success'] = False
            response['message'] = "Database Error"

    elif request.method == "DELETE":
        try:
            query = 'DELETE FROM NOTIFICATION WHERE user_id=' + user_id
            g.db.execute(query)
            g.db.commit()
            response['success'] = True
            response['message'] = "Successfully Deleted."
        except:
            response['success'] = False
            response['message'] = "Deletion Failed."

    return json.dumps(response)


if __name__ == '__main__':
    app.run(debug=True)
